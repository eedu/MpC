
from mpc.utils.rng import RNG
from mpc.utils.np import MyArray
from mpc.algorithms.square_and_multiply import exp_func


class GroupIterator:

    def __init__(self, group):
        self._group = group

        self._first_el = group._next(None)
        self._last_el = group._last()
        self._curr_el = None

    def __next__(self):
        if self._curr_el == self._last_el:
            raise StopIteration

        if self._curr_el is None:
            self._curr_el = self._first_el
        else:
            self._curr_el = self._group._next(self._curr_el)

        return self._curr_el


class Group():

    def __init__(self, elements=[]):
        self.elements = elements

    def __call__(self):
        return self['random']

    def __len__(self):
        return len(self.elements)

    def __getitem__(self, key):
        if type(key) is int:
            if key >= len(self.elements):
                return None

            return self.elements[key]
        if key == 'random':
            return RNG.choice(self.elements)

        raise RuntimeError()

    def __int__(self):
        return len(self.elements)

    def __iter__(self):
        return GroupIterator(self)

    def _is_valid_member(self, x):
        return x in self.elements

    def _last(self):
        return self.elements[-1]

    def _next(self, el):
        if el is None:
            return self.elements[0]

        if el == self.elements[-1]:
            return None

        i = self.elements.index(el)
        x = self.elements[i+1]

        return x


class Z(Group):

    def __init__(self, n):
        super().__init__()
        self.n = int(n)

    def __int__(self):
        return self.n

    def __getitem__(self, key):
        if key == 'random':
            rng = RNG(self.n, group=self)
            x = None

            while not self._is_valid_member(x):
                x = rng()

            return ZGroupMember(x, group=self)
        elif type(key) == int:
            return lambda: MyArray(self['random'] for x in range(key))

        return super().__getitem__(key)

    def _is_valid_member(self, x):
        return x is not None

    def _next(self, el):
        if el is None:
            x = 0
        else:
            x = el+1

        return ZGroupMember(x, group=self)

    def _last(self):
        return ZGroupMember(int(self) - 1, group=self)


class ZStar(Z):

    def __init__(self, n):
        super().__init__(n)

    def _is_valid_member(self, x):
        import math

        if not super()._is_valid_member(x):
            return False

        return int(x) > 0 and math.gcd(int(x), int(self)) == 1

    def _next(self, el):
        x = None

        while not self._is_valid_member(x):
            if x is None:
                x = el

            x = super()._next(x)

        return ZGroupMember(x, group=self)


class PQGroup(ZStar):

    def __init__(self, p, q, a=None):
        super().__init__(p)
        self.q = ZStar(q)

        if a is None:
            rng = RNG(p, group=self)
            a = (rng.order == int(q))()

        self.a = ZGroupMember(a, group=self, order=int(q))


class MPGroup(Group):

    def __init__(self, P, group):
        super().__init__(group)
        self.P = P
        self.group = group
        self.rng = RNG(P.rng(group), group=group)
        if isinstance(group, PQGroup):
            self.q = group.q
            self.a = group.a

    def __call__(self):
        return self.rng()

    def __mod__(self, other):
        return self.rng % other

    def __add__(self, other):
        return self.rng + other

    def __sub__(self, other):
        return self.rng - other

    def __mul__(self, other):
        return self.rng * other

    def __truediv__(self, other):
        return self.rng / other

    def __pow__(self, other):
        return self.rng ** other

    def __lt__(self, other):
        return self.rng < other

    def __le__(self, other):
        return self.rng <= other

    def __gt__(self, other):
        return self.rng > other

    def __ge__(self, other):
        return self.rng >= other

    def __eq__(self, other):
        return self.rng == other

    def __ne__(self, other):
        return self.rng != other

    def __getattr__(self, name):
        return self.rng.__getattr__(name)

    def __getitem__(self, key):
        return self.rng.__getitem__(key)


class ZGroupMember():

    def __init__(self, base, exp=1, group=None, order=None):
        self.has_order = False

        if group is not None:
            self.group = group

            if order is not None:
                self.order = order
                self.has_order = True

        elif isinstance(base, ZGroupMember):
            self.group = base.group
            if base.has_order:
                self.order = base.order
                self.has_order = True
        else:
            print("ZGroupMember has no group")
            raise RuntimeError

        exp = int(exp)  # Exp(exp)

        self.base = int(base)
        self.exp = int(exp)

        self._norm()
        self.value = int(self)

    def __hash__(self):
        return hash("{},{}".format(self.value, int(self.group)))

    def __getattr__(self, attr):
        if attr == 'order':
            self.order = self._calculate_order()
            self.has_order = True
            return self.order

    def __int__(self):
        if self.value is not None:
            return self.value

        def calculate_value(self):
            if self.exp < 0:
                self._norm()

            if self.exp == 0:
                return 1

            elif self.exp == 1:
                return int(self.base % int(self.group))

            else:
                self._norm(find_order=True)
                return exp_func(self.base, self.exp, self.group)

        self.value = int(calculate_value(self))
        return self.value

    def __mod__(self, other):
        if self.exp == 0 or self.exp == 1 or other == int(self.group):
            return self

        return int(self) % other

    def __add__(self, other):
        return ZGroupMember(int(self) + int(other), group=self.group)

    def __sub__(self, other):
        return ZGroupMember(int(self) - int(other), group=self.group)

    def __mul__(self, other):
        base = ZGroupMember(self.base, group=self.group)

        if isinstance(other, ZGroupMember) and other.base == base:
            return ZGroupMember(base, exp=self.exp + other.exp)

        other = int(other)

        if other == base:
            return ZGroupMember(base, exp=self.exp+1)

        return ZGroupMember(int(self) * other, group=self.group)

    def __truediv__(self, other):
        if isinstance(other, ZGroupMember):
            invother = ~other
        else:
            invother = ZGroupMember(other, exp=-1)

        return self * invother

    def __pow__(self, other):
        exp = self.exp * int(other)

        if not self.has_order:
            pass
            self.order
            # print("Tried to do pow without knowing order!")
            # raise RuntimeError

        base = self.base
        group = self.group
        order = self.order
        return ZGroupMember(base, exp=exp, group=group, order=order)

    def __invert__(self):
        base = ZGroupMember(self.base, group=self.group)
        return ZGroupMember(base, exp=-self.exp)

    def __neg__(self):
        return ZGroupMember(-int(self), group=self.group)

    def __repr__(self):

        if isinstance(self.base, ZGroupMember):
            b = "({})".format(self.base)
            b = b.replace(" mod {}".format(int(self.group)), "")
        else:
            b = self.base

        if self.exp == 0:
            b = "1"
            e = "   "
        elif self.exp > 1:
            import math
            spaces = " " * math.ceil(math.log10(self.group.n - self.exp))
            e = "^{}{}".format(self.exp, spaces)
        else:
            e = "   "

        return "{}{}mod {}".format(b, e, int(self.group))

    def __lt__(self, other):
        return int(self) < other

    def __le__(self, other):
        return int(self) <= other

    def __gt__(self, other):
        return int(self) > other

    def __ge__(self, other):
        return int(self) >= other

    def __eq__(self, other):
        if isinstance(other, ZGroupMember):
            if other.group == self.group and \
                    other.base == self.base and \
                    other.exp == self.exp:
                return True
        try:
            return int(self) == int(other)
        except Exception as e:
            print(e)
            return False

    def __ne__(self, other):
        return not (self == other)

    def _calculate_order(self):

        p = int(self.group)
        q = int((p - 1) / 2)
        g = self.base

        if int(g) == 0:
            return 0

        check_order = lambda x: exp_func(g, x, p) == 1

        if check_order(q):
            order = q
        elif check_order(p-1):
            order = p
        else:
            print("order was not q or p-1")
            print("g: {}, p: {}, q: {}".format(g, p, q))
            raise ValueError

        return order

    def _norm(self, find_order=False):
        self.base %= int(self.group)

        if self.exp < 0 or self.has_order or find_order:
            self.exp %= self.order

    def legendre(self):
        return self ** int(((int(self.group.n) - 1) / 2))

    def is_quadratic_residue(self):
        return self.legendre() == 1
