import numpy as np


def random_permutation(values):
    # TODO: implement secure version
    permuted = np.random.permutation(values)

    return list(permuted)
