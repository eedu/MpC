
def exp_func(x, y, n):
    x = int(x)
    y = int(y)
    exp = bin(y)
    value = x
    n = int(n)
 
    for i in range(3, len(exp)):
        value = value * value % n
        if(exp[i:i+1]=='1'):
            value = value * x % n

    return int(value)
