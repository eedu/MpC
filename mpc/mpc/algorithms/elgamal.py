
from mpc.group import Group
from mpc.utils.np import MyArray


class ElGamalCipher():

    def __init__(self, e1, e2):
        self.e1 = e1
        self.e2 = e2
        self.e2.order

    def __eq__(self, other):

        if not isinstance(other, ElGamalCipher):
            raise ValueError

        return self.e1 == other.e1 and self.e2 == other.e2

    def __mul__(self, other):
        e1 = self.e1
        e2 = self.e2

        if isinstance(other, ElGamalCipher):
            e1_ = other.e1
            e2_ = other.e2

            return ElGamalCipher(e1 * e1_, e2 * e2_)

        if isinstance(other, MyArray):
            return self * other.product()

        print(other)
        raise NotImplementedError

    def __div__(self, other):
        if isinstance(other, ElGamalCipher):
            other = other ** -1
            return self * other

        raise NotImplementedError

    def __pow__(self, other):
        e1 = self.e1
        e2 = self.e2

        return ElGamalCipher(e1 ** other, e2 ** other)

    def __repr__(self):
        return "({}, {})".format(self.e1, self.e2)


class ElGamal():

    def __init__(self, g, h_calculator, zq):
        h = h_calculator(g)
        self.encrypt = ElGamalEncrypt(g, h, zq)
        self.decrypt = (g, h_calculator)


class ElGamalEncrypt():

    def __init__(self, g, h, eph=None):
        self.g = g
        self.h = h
        self.eph = eph

    def __call__(self, m):
        g = self.g
        h = self.h
        r = self.eph() if isinstance(self.eph, Group) else self.eph

        marray = isinstance(m, MyArray)
        rarray = isinstance(r, MyArray)
        anyarray = marray or rarray

        ri = lambda i: r[i] if rarray else r
        mi = lambda i: m[i] if marray else m
        eg = lambda i: ElGamalCipher(g ** ri(i), (h ** ri(i)) * mi(i))

        if anyarray:
            cases = len(m) if marray else len(r)
            return MyArray(eg(i) for i in range(cases))

        return eg(None)

    def __getitem__(self, key):
        eph = key
        return ElGamalEncrypt(self.g, self.h, eph)


class ElGamalDecrypt():

    def __init__(self, g, h_calculator):
        self.g = g
        self.h_calculator = h_calculator

    def __call__(self, e):
        gr = e.e1
        mhr = e.e2

        gr_1 = gr ** -1
        hr_1 = self.h_calculator(gr_1)
        return mhr * hr_1
