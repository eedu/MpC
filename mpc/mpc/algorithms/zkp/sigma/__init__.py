
from mpc.utils.procedure import Procedure


class Sigma(Procedure):

    def __init__(self, prover, verifier, rounds):

        self.prover = prover
        self.verifier = verifier
        self.memories = {}
        self.memories[prover] = ShortMemory()
        self.memories[verifier] = ShortMemory()

        self.rounds = []
        for r in rounds:
            protagonist, func, func_args = r
            sigma_round = SigmaRound(protagonist, func)
            antagonist = prover if protagonist is verifier else verifier
            mem = self.memories[protagonist]
            self.rounds.append((sigma_round, mem, antagonist, func_args))

        super().__init__(self.run, None)

    def run(self):
        for r in self.rounds:
            sigma_round, mem, antagonist, func_args = r
            sigma_round(mem, antagonist, *func_args)


class SigmaRound(Procedure):

    def __init__(self, protagonist, func):
        super().__init__(func, protagonist)


class ShortMemory():

    def __init__(self):
        pass
