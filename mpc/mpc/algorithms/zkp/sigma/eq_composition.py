from . import Sigma
from mpc.utils.rng import RNG


class EQComposition(Sigma):
    '''
        Sigma EQ Composition
    '''

    def __init__(self, group, prover, verifier, g1, h1, g2, h2):
        announcement_args = [group, g1, h1, g2, h2]
        challenge_args = [group]
        response_args = []
        verify_args = [g1, h1, g2, h2]
        rounds = [(prover, announcement, announcement_args),
                  (verifier, challenge, challenge_args),
                  (prover, response, response_args),
                  (verifier, verify, verify_args)]
        super().__init__(prover, verifier, rounds)


def announcement(self, mem, verifier, group, g1, h1, g2, h2):
    ''' Runs protocol 49
        self = prover '''

    rng = RNG(group)
    mem.u = rng()

    a1 = g1 ** mem.u
    a2 = g2 ** mem.u

    self.publish((a1, a2), recipient=verifier)


def challenge(self, mem, prover, group):
    ''' Runs protocol 49
        self = verifier '''

    mem.a1, mem.a2 = self.read(sender=prover)

    rng = RNG(group)
    mem.c = rng()

    self.publish(mem.c, recipient=prover)


def response(self, mem, verifier):
    ''' Runs protocol 49
        self = prover '''

    c = self.read(sender=verifier)

    r = self._k*c + mem.u

    self.publish(r, recipient=verifier)


def verify(self, mem, prover, g1, h1, g2, h2):
    ''' Runs protocol 49
        self = verifier '''

    r = self.read(sender=prover)

    assert g1 ** r == mem.a1 * (h1 ** mem.c)
    assert g2 ** r == mem.a2 * h2 ** mem.c
