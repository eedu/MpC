
import types
import numpy as np


class MyArray():

    def ones(amount):
        return MyArray(1 for _ in range(amount))

    def zeros(amount):
        return MyArray(0 for _ in range(amount))

    def __init__(self, *elements):

        if isinstance(elements, tuple):
            if len(elements) == 0:
                self.elements = []
            else:
                gen = elements[0]

                if isinstance(gen, types.GeneratorType):
                    self.elements = []

                    while True:
                        try:
                            element = next(gen)
                            self.elements.append(element)
                        except StopIteration:
                            break

                elif isinstance(gen, list):
                    self.elements = gen
                elif isinstance(gen, tuple):
                    self.elements = list(gen)
                else:
                    print(elements)
                    print(gen)
                    raise ValueError
        else:
            print(elements)
            raise RuntimeError

    def __eq__(self, other):
        if not isinstance(other, MyArray) or len(self) != len(other):
            return False

        return all(self[i] == other[i] for i in range(len(self)))

    def __iter__(self):
        for el in self.elements:
            yield el

    def __add__(self, other):
        islist = type(other) == list or isinstance(other, MyArray)

        o = lambda i: (other[i] if islist else other)

        return MyArray(o(i) + el for i, el in enumerate(self))

    def __sub__(self, other):
        islist = type(other) == list or isinstance(other, MyArray)

        o = lambda i: (other[i] if islist else other)

        return MyArray(o(i) - el for i, el in enumerate(self))

    def __neg__(self):
        return MyArray(-el for el in self)

    def __mul__(self, other):
        islist = type(other) == list or isinstance(other, MyArray)

        o = lambda i: (other[i] if islist else other)

        return MyArray(o(i) * el for i, el in enumerate(self))

    def __div__(self, other):
        islist = type(other) == list or isinstance(other, MyArray)

        o = lambda i: (other[i] if islist else other)

        return MyArray(o(i) ** -1 * el for i, el in enumerate(self))

    def __pow__(self, other):
        islist = type(other) == list or isinstance(other, MyArray)

        o = lambda i: (other[i] if islist else other)

        return MyArray(el ** o(i) for i, el in enumerate(self))

    def __str__(self):
        return str(self.elements)

    def __len__(self):
        return len(self.elements)

    def __getitem__(self, key):
        if type(key) is int or type(key) is np.int64:
            if key >= len(self.elements):
                raise IndexError

            return self.elements[key]

        elif type(key) is slice:
            start = key.start if key.start is not None else 0
            stop = key.stop if key.stop else len(self)
            step = key.step if key.step is not None else 1

            return MyArray(el for i, el in enumerate(self) if i >= start
                           and i < stop and (i+start) % step == 0)
        elif callable(key):
            return MyArray(self[key(i)] for i in range(len(self)))

        else:
            print(key)
            print(type(key))
            raise NotImplementedError

        print(key)
        raise RuntimeError()

    def _last(self):
        return self.elements[-1]

    def _next(self, el):
        if el is None:
            return self.elements[0]

        if el == self.elements[-1]:
            return None

        i = self.elements.index(el)
        x = self.elements[i+1]

        return x

    def product(self):
        from functools import reduce
        return reduce(lambda x, y: x * y, self)

    def sum(self):
        from functools import reduce
        return reduce(lambda x, y: x + y, self)

    def append(self, el):
        self.elements.append(el)
