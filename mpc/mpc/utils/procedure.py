import mpc.utils as Utils

class Procedure():

    def __init__(self, func, boundto=None):
        self.name = self.__class__.__name__
        self.result = None
        self.func = func

        self.unbind()
        if boundto is not None:
            self.bindto(boundto)

    def __iter__(self):
        if type(self.func) is list:
            return self.func.__iter__()

        yield self.func

    def __str__(self):
        return self.name

    def __call__(self, *args, **kwargs):
        self.result = self._exec(self.func, *args, **kwargs)
        return self.result

    def _exec(self, func, *func_args, **func_kwargs):
        if self.boundto is None:
            return func(*func_args, **func_kwargs)

        return Utils.run_func_as_method(self.boundto, func, *func_args, **func_kwargs)

    def bindto(self, target):
        self.boundto = target

    def unbind(self):
        self.boundto = None
