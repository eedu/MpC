
from mpc.algorithms.trotter_johnson_unranking import random_permutation


class Permutation():

    def __init__(self, perm):
        if type(perm) == int:
            m = perm
            perm = random_permutation([x for x in range(m)])

        self.perm = perm
        self.m = len(perm)

    def __call__(self, i):
        return self.perm[i]

    def __neg__(self):
        return Permutation([self.perm.index(i) for i in range(self.m)])
