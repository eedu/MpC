
class Deck():

    def __init__(self, cards, Ci_1=None, k=None):
        self.cards = cards

        i = 0 if Ci_1 is None else Ci_1.i + 1 if k is None else Ci_1.i
        self.i = i

        if k is None:
            self.name = "Ci ({})".format(i)
        else:
            self.name = "Cik ({},{})".format(i,k)

    def __repr__(self):
        return self.name

    def __getitem__(self, key):
        return self.cards[key]
