import sys
from mpc.algorithms.miller_rabin import prime_test

# based on: http://stackoverflow.com/questions/1015307/python-bind-an-unbound-method#comment8431145_1015405
def bind(instance, func, as_name=None):
    """
    Bind the function *func* to *instance*, with either provided name *as_name*
    or the existing name of *func*. The provided *func* should accept the 
    instance as the first argument, i.e. "self".
    """
    if as_name is None:
        as_name = func.__name__
    bound_method = func.__get__(instance, instance.__class__)
    setattr(instance, as_name, bound_method)
    return bound_method

def run_func_as_method(instance, func, *func_args, **func_kwargs):
    if instance is None:
        raise RuntimeError()

    bind(instance, func, as_name='_temp')

    r = instance._temp(*func_args, **func_kwargs)

    if '_temp' in instance.__dict__:
        del instance.__dict__['_temp']

    return r

def is_prime(n):
    return prime_test(n)

def set_debug_mode():
    sys.setrecursionlimit(150)
