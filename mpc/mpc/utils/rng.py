import random
from mpc.utils.np import MyArray


def notf(t):
    return t is not False


class _QuantumArray():

    def __init__(self, condition, amount):
        self.condition = condition
        self.amount = amount

    def __call__(self, internal_call=False):
        return MyArray(self.condition() for _ in range(self.amount))


class _QuantumOp():
    def __init__(self, token, requires=[], op=None):
        self._token = token
        self._op = op
        self._requirements = requires if type(requires) is list else [requires]

    def __call__(self, internal_call=False):
        while True:
            token_res = self._token(internal_call=True)
            r, t = token_res

            if t is False:
                continue

            if any(x(r, t)[1] is False for x in self._requirements):
                continue

            res = self._op(r, t) if self._op else (r, t)
            res = res[0] if not internal_call else res
            return res

    def __mod__(self, o):
        return _QuantumOp(self, op=lambda x, t: (x, t % o))

    def __add__(self, o):
        return _QuantumOp(self, op=lambda x, t: (x, t + o))

    def __sub__(self, o):
        return _QuantumOp(self, op=lambda x, t: (x, t - o))

    def __mul__(self, o):
        return _QuantumOp(self, op=lambda x, t: (x, t * o))

    def __truediv__(self, o):
        return _QuantumOp(self, op=lambda x, t: (x, t / o))

    def __pow__(self, o):
        return _QuantumOp(self, op=lambda x, t: (x, t ** o))

    def __lt__(self, o):
        return _QuantumOp(self, requires=lambda x, t: (x, t < o))

    def __le__(self, o):
        return _QuantumOp(self, requires=lambda x, t: (x, t <= o))

    def __gt__(self, o):
        return _QuantumOp(self, requires=lambda x, t: (x, t > o))

    def __ge__(self, o):
        return _QuantumOp(self, requires=lambda x, t: (x, t >= o))

    def __eq__(self, o):
        return _QuantumOp(self, requires=lambda x, t: (x, t == o))

    def __ne__(self, o):
        return _QuantumOp(self, requires=lambda x, t: (x, t != o))

    def __bool__(self):
        return _QuantumOp(self, requires=lambda x, t: (x, notf(t)))

    def __and__(self, o):
        return _QuantumOp(self, requires=lambda x, t: (x, notf(t) and notf(o)))

    def __or__(self, o):
        return _QuantumOp(self, requires=lambda x, t: (x, notf(t) or notf(o)))

    def __getitem__(self, key):
        if type(key) is int:
            amount = key
            return _QuantumArray(self, amount)
        elif type(key) is slice:
            start = key.start if key.start is not None else 0
            stop = key.stop
            step = key.step if key.step is not None else 1

            return _QuantumOp(self,
                              requires=lambda x, t: (x,
                                                     (t >= start
                                                      and t < stop
                                                      and (t-start) % step == 0
                                                      )))
        else:
            print(key)
            raise NotImplementedError

    def __getattr__(self, key):
        return _QuantumOp(self, op=lambda x, t: (x, t.__getattr__(key)))


class _QuantumObservation():

    def __init__(self, value):
        self.value = value

    def __bool__(self):
        return type(self.value) is not bool or self.value


class _QuantumState():

    def __init__(self, rng):
        self.rng = rng
        self.value = None
        pass

    def __call__(self, internal_call=False):
        v = self.rng.sample()
        return v


class RNG():

    def __init__(self, v, group=None):
        self.origin = type(v)
        self.group = group

        def get_generator():
            from mpc.group import Group

            if self.origin is list:
                return lambda: random.choice(v)
            elif self.origin is int:
                return lambda: random.choice(range(v))
            elif isinstance(v, Group):
                # TODO: implement secure RNG
                return v
            elif self.origin == type(len):
                return v
            elif self.origin == type(lambda x: None):
                return v
            else:
                print("Not implemented")
                raise NotImplementedError

        generator = get_generator()

        if group is not None:
            from mpc.group import ZGroupMember
            self.generator = lambda: ZGroupMember(generator(), group=group)
        else:
            self.generator = generator

    def __call__(self, internal_call=False):
        ret = _QuantumOp(self.token)()

        if type(ret) is tuple:
            return ret[0]

        return ret

    def __mod__(self, other):
        return _QuantumOp(self.token) % other

    def __add__(self, other):
        return _QuantumOp(self.token) + other

    def __sub__(self, other):
        return _QuantumOp(self.token) - other

    def __mul__(self, other):
        return _QuantumOp(self.token) * other

    def __truediv__(self, other):
        return _QuantumOp(self.token) / other

    def __pow__(self, other):
        return _QuantumOp(self.token) ** other

    def __lt__(self, other):
        return _QuantumOp(self.token) < other

    def __le__(self, other):
        return _QuantumOp(self.token) <= other

    def __gt__(self, other):
        return _QuantumOp(self.token) > other

    def __ge__(self, other):
        return _QuantumOp(self.token) >= other

    def __eq__(self, other):
        return _QuantumOp(self.token) == other

    def __ne__(self, other):
        return _QuantumOp(self.token) != other

    def __getattr__(self, name):
        if name == 'token':
            return _QuantumState(self)

        return _QuantumOp(self.token).__getattr__(name)

    def __getitem__(self, key):
        return _QuantumOp(self.token).__getitem__(key)

    def sample(self):
        v = self.generator()
        return v, v
