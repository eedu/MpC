from mpc.participant.multiparty import Multiparty
from mpc.participant.player import Player


class Participants(Multiparty):

    def __init__(self, n, pqgroup, a, s):
        # public storage
        self.n = n
        self.a = a
        self.s = s

        players = [Player(self, x) for x in range(n)]

        super().__init__(players, pqgroup)
