from mpc.utils.procedure import Procedure

from mpc.protocol_suites.castella_roca.const import m

class Procedure33(Procedure):

    def __init__(self, boundto):
        super().__init__(run, boundto)

def run(self, Ca, Cb, R, PI):
    ''' Runs procedure 33 
        self = Participants '''


    for j in range(m):
        cbj = Cb[j]
        dbj, abj = cbj

        CaPIj = Ca[PI(j)]
        daPIj, aaPIj = CaPIj

        rPIj = R[PI(j)]

        assert dbj == daPIj ** rPIj
        assert abj == aaPIj ** rPIj
