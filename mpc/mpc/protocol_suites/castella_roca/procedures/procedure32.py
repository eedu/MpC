from mpc.utils.procedure import Procedure

from mpc.utils.rng import RNG
from mpc.utils.mental_poker.deck import Deck
from mpc.algorithms.trotter_johnson_unranking import random_permutation

from mpc.protocol_suites.castella_roca.const import m


class Procedure32(Procedure):

    def __init__(self, boundto):
        super().__init__(run, boundto)


def run(self, C, k=None):
    ''' runs as Pi '''

    group = self.group
    q = group.q

    rng = RNG(q)
    R = [rng[3:q:2]() for _ in range(m)]

    enc = lambda dj, aj, rj: (dj ** rj, aj ** rj)
    enc_args = lambda cj, j: (cj[0], cj[1], R[j])

    c_ = [enc(*enc_args(cj, j)) for j, cj in enumerate(C)]

    perm = random_permutation([x for x in range(m)])
    perm_func = lambda x: perm[x]
    PI = perm_func

    Cstar = Deck([c_[PI(j)] for j, _ in enumerate(c_)], Ci_1=C, k=k)

    return Cstar, R, PI
