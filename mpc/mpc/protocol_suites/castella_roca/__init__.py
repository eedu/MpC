from mpc.protocol_suites.castella_roca.participants import Participants

import mpc.protocol_suites.castella_roca.stages as stages
import mpc.utils as Utils


class CastellaRoca():

    def __init__(self, n, p, q, a, s):
        group = a.group
        self.P = Participants(n, group, a, s)
        self.a = a
        self.q = q
        self.initialization = Utils.bind(self.P, stages.initialization)
        self.card_shuffle = Utils.bind(self.P, stages.card_shuffle)
        self.card_draw = Utils.bind(self.P, stages.card_draw)
        self.card_open = Utils.bind(self.P, stages.card_open)
        self.card_discard = Utils.bind(self.P, stages.card_discard)
