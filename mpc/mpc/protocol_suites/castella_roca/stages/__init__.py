from .initialization import initialization as stage01
from .card_shuffle import card_shuffle as stage02
from .card_draw import card_draw as stage03
from .card_open import card_open as stage04
from .card_discard import card_discard as stage05


def initialization(*args, **kwargs):
    stage01(*args, **kwargs)


def card_shuffle(*args, **kwargs):
    stage02(*args, **kwargs)


def card_draw(*args, **kwargs):
    stage03(*args, **kwargs)


def card_open(*args, **kwargs):
    stage04(*args, **kwargs)


def card_discard(*args, **kwargs):
    stage05(*args, **kwargs)
