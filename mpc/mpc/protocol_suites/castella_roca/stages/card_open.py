
from mpc.protocol_suites.castella_roca.protocols.protocol50 import Protocol50

def card_open(self, player_index, card_index):
    P = self

    Pu = P[player_index]

    p50 = Protocol50(P)
    p50(Pu, card_index)
