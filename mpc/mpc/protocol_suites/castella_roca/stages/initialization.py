
from mpc.utils.rng import RNG
from mpc.group import ZGroupMember, Z

from mpc.protocol_suites.castella_roca.const import m
from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_X
from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_B


def initialization(self):
    ''' here self is Participants '''

    P = self
    q = P.group.q

    last = P[-1]

    def store_publickey(self, Pi):
        '''here, self is Participants '''
        i, Bi = self.read(sender=Pi)
        self.store("B_{}".format(i), Bi)

    def init_keypair(self):
        '''here, self is player Pi '''

        i = int(self)

        a = self.a

        rng = RNG(q)

        self._k = (rng % 2 == 1)()
        qgroup = Z(q)
        self._k_1 = int(ZGroupMember(self._k, group=qgroup, order=q-1) ** -1)
        # could also use pow(self._k, -1, q)
        self.b = self.k(a)

        self.publish((i, self.b), recipient=all)
        P.compute(store_publickey, self)

    P.compute(init_keypair)

    rng = RNG(q)
    X = rng[3:q:2][m]()
    # TODO: Implement secure generation of X
    P.store(STORAGE_KEY_X, X)

    def compute_B(self):
        ''' here self = Pi '''
        i = int(self)

        # TODO: implement secure generation of B

        next_player = self.next()
        recipient = next_player or all

        if i == 0:
            next_acc = self.b
        else:
            last_player = self.previous()
            acc = self.read(sender=last_player)
            next_acc = self.k(acc)

        self.publish(next_acc, recipient)

    P.compute(compute_B)

    B = P.read(sender=last)
    P.store(STORAGE_KEY_B, B)
