
from mpc.utils.mental_poker.deck import Deck
from mpc.protocol_suites.castella_roca.protocols.protocol47 import Protocol47

from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_X
from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_B
from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_DECK
from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_SHUFFLED_DECK

def card_shuffle(self):
    ''' here self is Participants '''

    P = self
    a = self.a

    X = P.retrieve(STORAGE_KEY_X)
    B = P.retrieve(STORAGE_KEY_B)

    C0 = [(a ** x, B) for x in X]

    deck = Deck(C0)
    P.store(STORAGE_KEY_DECK, deck)

    p47 = Protocol47(P)
    Cn = p47()
    P.store(STORAGE_KEY_SHUFFLED_DECK, Cn)
