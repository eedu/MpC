
from mpc.protocol_suites.castella_roca.protocols.protocol49 import Protocol49

def card_draw(self, player_index, card_index):
    P = self

    Pu = P[player_index]

    p49 = Protocol49(P)
    p49(Pu, card_index)
