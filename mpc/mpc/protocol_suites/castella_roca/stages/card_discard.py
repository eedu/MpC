
from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_SHUFFLED_DECK
from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_DISCARDED_CARDS

def card_discard(self, player_index, card_index):
    P = self

    shuffled_cards = P.retrieve(STORAGE_KEY_SHUFFLED_DECK)
    discarded_cards = P.retrieve(STORAGE_KEY_DISCARDED_CARDS) or []
    card = shuffled_cards[card_index]
    discarded_cards.append(card)
    P.store(STORAGE_KEY_DISCARDED_CARDS, discarded_cards)
