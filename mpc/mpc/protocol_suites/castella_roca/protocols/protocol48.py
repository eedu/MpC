from mpc.utils.procedure import Procedure

from mpc.utils.rng import RNG
from ..procedures.procedure32 import Procedure32
from ..procedures.procedure33 import Procedure33

from mpc.protocol_suites.castella_roca.const import m


class Protocol48(Procedure):
    '''
        Shuffling verification protocol
        page 144
    '''

    def __init__(self, suite):
        super().__init__(run, suite)


def run(self, Pi, Ci_1, Ci, s):
    ''' Runs protocol 48
        self = Participants '''

    P = self
    Ciks = []

    def produce_Cik(self, Ci, k):
        ''' Executed by Pi
            Generates another encryption of Ci -> Cik
        '''

        proc32 = Procedure32(self)
        Cik, Rik, PIik = proc32(Ci, k=k)

        exec_name = "prot48_proc32_k{}".format(k)
        self.privately_store(exec_name, (Rik, PIik))
        self.publish(Cik, recipient=all)

    # part 1
    for k in range(s):
        Pi.compute(produce_Cik, Ci, k)
        Cik = P.read(sender=Pi)
        Ciks.append(Cik)

    # part 2
    rng = RNG(P)
    u = rng[0:2][s]()
    u[0] = 0

    def prove_Cik(self, Ci, Cik, k, uk):
        ''' Executed by Pi
            Returns either (Rik, PIik) or (R'ik, PI'ik), depending on value uk
        '''
        exec_name = "prot48_proc32_k{}".format(k)
        Rik, PIik = self.privately_retrieve(exec_name)

        if uk == 1:
            self.publish((Rik, PIik), recipient=all)
        elif uk == 0:
            Ri, PIi = self.privately_retrieve('prot47_proc32')

            inverse = lambda fx: lambda x: [fx(y) for y in range(m)].index(x)
            compose = lambda fx, fy: lambda x: fx(fy(x))

            PI_ik = compose(PIi, PIik)
            R_ik = [Rik[inverse(PIi)(y)] * Ri[y] for y in range(m)]
            self.publish((R_ik, PI_ik), recipient=all)

        else:
            print(uk)
            raise NotImplementedError

    proc33 = Procedure33(self)
    for k, uk in enumerate(u):
        Cik = Ciks[k]
        if uk == 1:
            Pi.compute(prove_Cik, Ci, Cik, k, uk)
            Rik, PIik = P.read(sender=Pi)
            proc33(Ci, Cik, Rik, PIik)
        elif uk == 0:
            Pi.compute(prove_Cik, Ci, Cik, k, uk)
            R_ik, PI_ik = P.read(sender=Pi)
            proc33(Ci_1, Cik, R_ik, PI_ik)
        else:
            print(uk)
            raise NotImplementedError
