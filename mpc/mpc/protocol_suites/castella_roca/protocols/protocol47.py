from mpc.utils.procedure import Procedure

from .protocol48 import Protocol48
from ..procedures.procedure32 import Procedure32

from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_DECK


class Protocol47(Procedure):
    '''
        Card shuffling protocol
        page 143
    '''

    def __init__(self, multiparty):
        super().__init__(run, multiparty)


def run(self):
    ''' Runs protocol 47
        self = Participants '''

    P = self
    last = P[-1]

    def private_func(self):
        ''' Here, self = Pi '''

        i = self.id
        recipient = all if self == last else P[i+1]
        sender = all if i == 0 else P[i-1]

        if i == 0:
            Ci_1 = P.retrieve(STORAGE_KEY_DECK)
        else:
            Ci_1 = self.read(sender)

        # Procedure 32 generates next iteration of shuffling
        proc32 = Procedure32(self)
        Ci, Ri, PIi = proc32(Ci_1)

        # Storing Ri and PIi of Ci
        self.privately_store('prot47_proc32', (Ri, PIi))

        # Protocol 48 proves that Ci has been properly computed
        # The protocol aborts in case of failure
        Pi, s = self, self.s
        prot48 = Protocol48(P)
        prot48(Pi, Ci_1, Ci, s)

        self.publish(Ci, recipient)

    P.compute(private_func)

    Cn = P.read(sender=last)

    for cj in Cn:
        dnj, anj = cj
        if dnj.is_quadratic_residue() and anj.is_quadratic_residue():
            continue
        else:
            print("Quadratic card-marking spotted!")
            raise RuntimeError

    return Cn
