from mpc.utils.procedure import Procedure

from mpc.participant.multiparty import Multiparty
from mpc.algorithms.zero_knowledge_proofs.sigma.eq_composition import EQComposition

from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_X
from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_SHUFFLED_DECK


class Protocol49(Procedure):
    '''
        Card drawing protocol
        page 145
    '''

    def __init__(self, suite):
        super().__init__(run, suite)


def run(self, Pu, j):
    ''' Runs protocol 49
        self = Participants '''

    P = self

    def Pu_draw_card(self):
        ''' Here, self = Pu '''

        Cn = P.retrieve(STORAGE_KEY_SHUFFLED_DECK)
        cnj = Cn[j]
        dnj, anj = cnj

        def publish_er(self):
            ''' Here, self = Pi
                Pi != Pu
            '''
            Pi = self
            i = int(self)

            er_1 = P.read()
            er = Pi.k_1(er_1)

            # Zero knowledge proof that er^Ki == er_1
            Bi = P.retrieve("B_{}".format(i))
            group = self.group
            prover = self
            verifier = Multiparty([Pj for Pj in P if Pj != Pi], group)
            g1 = P.a
            h1 = Bi
            g2 = er
            h2 = er_1

            proof = EQComposition(group, prover, verifier, g1, h1, g2, h2)
            proof()

            self.publish(er)

        e0 = anj
        self.publish(e0)

        for Pi in (Pi for Pi in reversed(P) if Pi != Pu):
            Pi.compute(publish_er)

        er = P.read()

        # Used for zero knowledge proof in protocol 50
        P.store("Proof_Card_{}".format(j), er)

        en = self.k_1(er)
        X = P.retrieve(STORAGE_KEY_X)
        drawn_card = None

        for x in X:
            if dnj == en ** x:
                drawn_card = x
                break

        if drawn_card is None:
            print("No card found")
            raise ValueError

        self.privately_store("Card_{}".format(j), (en, x))

    Pu.compute(Pu_draw_card)
