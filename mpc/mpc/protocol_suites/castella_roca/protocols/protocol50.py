from mpc.utils.procedure import Procedure

from mpc.participant.multiparty import Multiparty
from mpc.algorithms.zero_knowledge_proofs.sigma.eq_composition import EQComposition

from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_X
from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_SHUFFLED_DECK
from mpc.protocol_suites.castella_roca.const import STORAGE_KEY_DISCARDED_CARDS


class Protocol50(Procedure):
    '''
        Card opening protocol
        page 145
    '''

    def __init__(self, suite):
        super().__init__(run, suite)


def run(self, Pu, j):
    ''' Runs protocol 50
        self = Participants '''

    P = self

    def Pu_open_card(self):
        ''' Here, self = Pu '''

        Pu = self
        u = int(self)

        en, x = self.privately_retrieve("Card_{}".format(j))

        self.publish((j, en, x), recipient=all)

        def verify_x(self):
            ''' Here, self = Pi
                Pi != Pu
            '''

            j, en, x = self.read(sender=Pu)

            Cn = P.retrieve(STORAGE_KEY_SHUFFLED_DECK)
            cnj = Cn[j]
            discarded_cards = P.retrieve(STORAGE_KEY_DISCARDED_CARDS) or []
            assert cnj not in discarded_cards

            X = P.retrieve(STORAGE_KEY_X)
            assert x in X

            dnj, anj = cnj
            assert dnj == en ** x

        for Pi in (Pi for Pi in P if Pi != Pu):
            Pi.compute(verify_x)

        # Saved during protocol 49
        en_1 = P.retrieve("Proof_Card_{}".format(j))

        # Zero knowledge proof that en^K_u == en_1
        Bu = P.retrieve("B_{}".format(u))
        group = self.group
        prover = self
        verifier = Multiparty([Pj for Pj in P if Pj != Pu], group)
        g1 = P.a
        h1 = Bu
        g2 = en
        h2 = en_1

        proof = EQComposition(group, prover, verifier, g1, h1, g2, h2)
        proof()

    Pu.compute(Pu_open_card)



