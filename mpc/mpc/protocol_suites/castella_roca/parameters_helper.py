
import sympy
import mpc.utils as Utils
from mpc.group import PQGroup, ZGroupMember


def pq():

    a = 100
    b = 5000

    while True:
        q = sympy.randprime(a, b)
        p = q * 2 + 1
        if Utils.is_prime(p):
            return p, q


p, q = pq()


class Generator(ZGroupMember):

    def __init__(self, group, order):
        a = None

        while True:
            a = group['random']

            # TODO: figure out why the paper asks for non-quadratic residue
            # of order q, when we have
            #
            #         {  0, if p|a
            # (a/p) = {  1, if a in Qp
            #         { -1, if a in _Qp
            # (a/p) being the legendre symbol
            #
            # (a/p) is computed by doing a^{(p-1)/2} mod p
            # since p=2q+1, a^{(p-1)/2} = a^q
            # we need order q, so a^q = 1
            # therefore a is quadratic residue

            if a.order == order and a.is_quadratic_residue():
                break

        super().__init__(int(a), exp=1, group=group, order=order)


a = Generator(PQGroup(p, q), q)
s = 10
