
from collections import Iterable
from mpc.utils.np import MyArray


class CK():

    def __init__(self, z):
        self.z = z
        self.h = self.get_generator()
        self.generators = MyArray()
        self.add_gs(1)

    def __call__(self, values, r):
        return self.commit(values, r)

    def commit(self, values, r):
        if not isinstance(values, Iterable):
            values = [values]

        if len(values) > len(self.generators):
            self.add_gs(len(values) - len(self.generators))

        generators = self.generators[:len(values)]
        commit = self.h ** r * (generators ** values).product()

        commit.order
        return commit

    def add_gs(self, n):
        for i in range(n):
            a = self.get_generator()
            self.generators.append(a)

    def get_generator(self):
        zq = self.z.q
        a = self.z.a
        return a ** zq()
