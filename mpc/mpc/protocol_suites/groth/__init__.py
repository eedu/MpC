
from mpc.protocol_suites.groth.participants import Participants
import mpc.utils as Utils
import mpc.protocol_suites.groth.stages as stages


class Groth():

    def __init__(self, n, group):
        self.P = Participants(n, group)
        self.poce = Utils.bind(self.P, stages.poce)
        self.cpoke = Utils.bind(self.P, stages.cpoke)
        self.metce = Utils.bind(self.P, stages.metce)
        self.shuffle = Utils.bind(self.P, stages.shuffle)
