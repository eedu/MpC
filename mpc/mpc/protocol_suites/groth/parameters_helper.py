
from mpc.group import PQGroup


def pq():
    import sympy
    import mpc.utils as Utils

    a = 100000000000000
    b = 5000000000000000

    while True:
        q = sympy.randprime(a, b)
        p = q * 2 + 1
        if Utils.is_prime(p):
            return p, q


p = 7726431855836867
q = 3863215927918433
a = 5580511202994430

group = PQGroup(p, q, a)

if __name__ == '__main__':
    p, q = pq()
    a = None
    group = PQGroup(p, q, a)
    print("p = {}\nq = {}\na = {}".format(p, q, a))
    exit()
