
from mpc.participant.multiparty import Multiparty
from mpc.participant.player import Player
from mpc.protocol_suites.groth.ck import CK
from mpc.group import MPGroup, ZGroupMember
from mpc.algorithms.elgamal import ElGamal
from mpc.algorithms.zkp.sigma.eq_composition import EQComposition


class Participants(Multiparty):

    def __init__(self, n, pqgroup):
        players = [Player(self, x) for x in range(n)]
        self.n = n
        super().__init__(players, pqgroup)

        P = self
        zpMP = MPGroup(self, pqgroup)
        q = pqgroup.q

        P.ck = CK(zpMP)
        P.a = pqgroup.a
        zq = q

        last = P[-1]

        def init_keypair(self):
            '''here, self is player Pi '''

            i = int(self)

            a = self.a

            self._k = zq()
            self._k_1 = ZGroupMember(self._k, group=zq, order=int(q)-1)
            self._k_1 = int(self._k_1 ** -1)
            self.b = self.k(a)

            self.publish((i, self.b), recipient=all)
            P.store("B_{}".format(i), self.b)

        P.compute(init_keypair)

        def B_computer(self):
            ''' here self = Pi '''
            i = int(self)

            # TODO: implement secure generation of B

            next_player = self.next()
            recipient = next_player or all

            if i == 0:
                next_acc = self.b
            else:
                last_player = self.previous()
                acc = self.read(sender=last_player)
                next_acc = self.k(acc)

            self.publish(next_acc, recipient)

        P.compute(B_computer)

        self.B = P.read(sender=last)

        def h_computer(gr):

            def h_computer_pi(self):
                ''' here self = Pi '''

                i = int(self)

                next_player = self.next()
                recipient = next_player or all

                if i == 0:
                    acc = gr
                else:
                    last_player = self.previous()
                    acc = self.read(sender=last_player)

                g1 = self.a
                h1 = self.b
                g2 = acc
                h2 = self.k(g2)

                proof = EQComposition(pqgroup, self, P, g1, h1, g2, h2)
                proof()

                self.publish(h2, recipient)

            P.compute(h_computer_pi)
            hr = P.read(sender=last)
            return hr

        self.elgamal = ElGamal(self.a, h_computer, zq)
