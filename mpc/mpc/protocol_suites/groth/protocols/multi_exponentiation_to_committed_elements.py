
from mpc.algorithms.zkp.sigma import Sigma

from ..const import m


class MEtCE(Sigma):
    '''
        Multi Exponentiation to Committed Elements
    '''

    def __init__(self, prover, verifier, A, E_, E, a_, r, R):
        announcement_args = [A, E_, E, a_, r, R]
        challenge_args = []
        response_args = [a_, r, R, E_]
        verification_args = [A, E_, E]
        rounds = [(prover, announcement, announcement_args),
                  (verifier, challenge, challenge_args),
                  (prover, response, response_args),
                  (verifier, verification, verification_args)]

        super().__init__(prover, verifier, rounds)


def announcement(self, mem, verifier, A, E_, E, a_, r, R):

    zq = self.group.q
    Epk = self.elgamal.encrypt
    Rck = zq
    Rpk = zq
    com_ck = self.ck

    assert A == com_ck(a_, r)
    assert E == Epk[R](1) * E_ ** a_

    mem.a_0 = zq[m]()
    mem.r_0 = Rck()
    A_0 = com_ck(mem.a_0, mem.r_0)
    mem.F_1 = zq()
    mem.r_01, mem.r_11 = Rck[2]()

    C_01 = com_ck(mem.F_1, mem.r_01)
    C_11 = com_ck(mem.F_1*0, mem.r_11)

    mem.R_01 = Rpk()
    mem.R_11 = R

    D_01 = Epk[mem.R_01](self.a ** mem.F_1) * E_ ** mem.a_0

    self.publish((A_0, C_01, C_11, D_01), recipient=verifier)


def challenge(self, mem, prover):
    zq = self.group.q

    mem.A_0, mem.C_01, mem.C_11, mem.D_01 = self.read(sender=prover)

    mem.t = zq()

    self.publish(mem.t, recipient=prover)


def response(self, mem, verifier, a_, r, R, E_):
    t = self.read(sender=verifier)

    f_ = mem.a_0 + a_ * t

    z = mem.r_0 + r * t
    z_1 = mem.r_01 + mem.r_11 * t
    Z_1 = mem.R_01 + R * t

    self.publish((f_, mem.F_1, z, z_1, Z_1), recipient=verifier)


def verification(self, mem, prover, A, E_, E):
    com_ck = self.ck
    Epk = self.elgamal.encrypt

    f_, F_1, z, z_1, Z_1 = self.read(sender=prover)

    assert mem.A_0 * A ** mem.t == com_ck(f_, z)
    assert mem.C_01 * mem.C_11 ** mem.t == com_ck(F_1, z_1)
    assert Epk[Z_1](self.a ** F_1) * E_ ** f_ == mem.D_01 * E ** mem.t
