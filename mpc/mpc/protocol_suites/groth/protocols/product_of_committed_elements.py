from mpc.algorithms.zkp.sigma import Sigma

from ..const import m
from mpc.utils.rng import RNG
from mpc.utils.np import MyArray
from mpc.group import ZGroupMember


class PoCE(Sigma):
    '''
        Product of committed elements
        x_1, ..., x_n
        commit on x = x_1 * ... * x_n
    '''

    def __init__(self, prover, verifier, a, A, a_, r):
        announcement_args = [a, a_, A, r]
        challenge_args = []
        response_args = [a_, A, r]
        verification_args = [a, A]
        rounds = [(prover, announcement, announcement_args),
                  (verifier, challenge, challenge_args),
                  (prover, response, response_args),
                  (verifier, verification, verification_args)]
        super().__init__(prover, verifier, rounds)


def announcement(self, mem, verifier, a, a_, A, r):
    ''' Runs protocol
        self = prover
        A = com(a_1, ... a_n; r)

    '''

    group = self.group.q

    q = group
    zq = RNG(q, group=group)
    Rck = RNG(q, group=group)
    com_ck = self.ck

    assert a == a_.product()
    assert A == com_ck(a_, r)

    mem.a_0 = zq[m]()
    mem.r_0 = Rck()
    # a01, ..., a0n; r0
    mem.A0 = com_ck(mem.a_0, mem.r_0)

    mem.b_ = MyArray([ZGroupMember(1, group=group)])
    for i in range(m):
        mem.b_.append(mem.b_[i] * a_[i])

    # b00 ... b0n
    mem.b_0 = zq[m+1]()
    mem.r_b0 = Rck()
    mem.r_b1 = Rck()

    # b01, ..., b0n; rb0
    mem.B0 = com_ck(mem.b_0[1:], mem.r_b0)

    # b11, ..., b1n; rb1
    mem.B = com_ck(mem.b_[1:], mem.r_b1)

    mem.r_0prime = Rck()
    # b00; r0'
    mem.B_0prime = com_ck(mem.b_0[0], mem.r_0prime)

    mem.r_circumflex = Rck()
    # b0n; r^
    mem.B_circumflex = com_ck(mem.b_0[-1], mem.r_circumflex)

    mem.r_00, mem.r_01, mem.r_10 = Rck[3]()
    mem.r_11 = mem.r_b1

    # a01b00, ..., a0nb0n-1; r00
    c00 = mem.a_0 * mem.b_0
    mem.C00 = com_ck(c00, mem.r_00)

    # a01b0, ..., a0nbn-1; r01
    c01 = mem.a_0 * mem.b_
    mem.C01 = com_ck(c01, mem.r_01)

    # a1b00, ..., anb0n-1; r10
    c10 = a_ * mem.b_0
    mem.C10 = com_ck(c10, mem.r_10)

    # a1b0, ..., anbn-1; r11
    c11 = a_ * mem.b_
    mem.C11 = com_ck(c11, mem.r_11)

    assert mem.B == mem.C11

    self.publish((mem.A0, mem.B0, mem.B_0prime, mem.B_circumflex, mem.C00,
                 mem.C01, mem.C10, mem.C11), recipient=verifier)


def challenge(self, mem, prover):
    ''' Here, self = verifier '''

    group = self.group.q
    q = group
    zq = RNG(q, group=group)

    msg = self.read(sender=prover)
    mem.A0 = msg[0]
    mem.B0 = msg[1]
    mem.B_0prime = msg[2]
    mem.B_circumflex = msg[3]
    mem.C00 = msg[4]
    mem.C01 = msg[5]
    mem.C10 = msg[6]
    mem.C11 = msg[7]

    mem.s, mem.t = zq[2]()
    self.publish((mem.s, mem.t), recipient=prover)


def response(self, mem, verifier, a_, A, r):
    s, t = self.read(sender=verifier)

    # a01 + s*a1, ..., a0n + s*an;
    f_ = mem.a_0 + a_ * s
    # b00 + t*b0, ..., b0n + t*bn;
    F_ = mem.b_0 + mem.b_ * t
    z = mem.r_0 + s * r
    z_b = mem.r_b0 + t * mem.r_b1
    z_prime = mem.r_0prime
    z_circumflex = mem.r_circumflex
    z_ab = mem.r_00 + (s * mem.r_10) + (t * mem.r_01) + (s * t * mem.r_11)

    self.publish((f_, F_, z, z_b, z_prime, z_circumflex, z_ab),
                 recipient=verifier)


def verification(self, mem, prover, a, A):
    com_ck = self.ck

    f_, F_, z, z_b, z_prime, z_circumflex, z_ab = self.read(sender=prover)

    assert mem.A0 * (A ** mem.s) == com_ck(f_, z)

    B = mem.C11
    assert mem.B0 * (B ** mem.t) == com_ck(F_[1:], z_b)

    B_prime = com_ck(1, 0)
    assert mem.B_0prime * (B_prime ** mem.t) == com_ck(F_[0], z_prime)

    assert mem.B_circumflex == com_ck(F_[-1] - mem.t*a, z_circumflex)

    Cs = mem.C00
    Cs = Cs * (mem.C01 ** mem.t)
    Cs = Cs * (mem.C10 ** mem.s)
    Cs = Cs * (mem.C11 ** (mem.s * mem.t))
    assert Cs == com_ck(f_ * F_, z_ab)
