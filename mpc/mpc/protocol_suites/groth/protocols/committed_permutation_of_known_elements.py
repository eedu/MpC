from mpc.algorithms.zkp.sigma import Sigma

from mpc.group import ZGroupMember
from mpc.utils.np import MyArray

from .product_of_committed_elements import PoCE


class CPoKE(Sigma):
    '''
        Committed Permutation of Known Elements
    '''

    def __init__(self, prover, verifier, B, a_, PI, r):
        challenge_args = [B, a_]
        response_args = [B, a_, PI, r]
        rounds = [(verifier, challenge, challenge_args),
                  (prover, response, response_args)]
        super().__init__(prover, verifier, rounds)


def challenge(self, mem, prover, B, a_):
    zq = self.group.q

    mem.x = zq()

    a2_ = -a_ + mem.x
    mem.a = a2_.product()

    self.publish((mem.a, mem.x), recipient=prover)


def response(self, mem, verifier, B, a_, PI, r):
    com_ck = self.ck
    zp = self.group

    a, x = self.read(sender=verifier)
    x_ = MyArray.zeros(len(a_)) + x
    B_prime = ZGroupMember(com_ck(x_, 0) * (B**-1),
                           group=zp, order=B.order)

    b_ = -a_[PI] + x

    assert com_ck(b_, -r) == B_prime

    poce = PoCE(self, verifier, a, B_prime, b_, -r)

    poce()
