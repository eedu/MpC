from mpc.algorithms.zkp.sigma import Sigma
from mpc.utils.np import MyArray

from ..const import m
from .committed_permutation_of_known_elements import CPoKE
from .multi_exponentiation_to_committed_elements import MEtCE


class Shuffle(Sigma):
    '''
        Shuffle
    '''

    def __init__(self, prover, verifier, e_, E_, PI, R_):
        announcement_args = [e_, E_, PI, R_]
        challenge1_args = []
        response1_args = [PI]
        challenge2_args = []
        response2_args = [e_, E_, PI, R_]
        rounds = [(prover, announcement, announcement_args),
                  (verifier, challenge1, challenge1_args),
                  (prover, response1, response1_args),
                  (verifier, challenge2, challenge2_args),
                  (prover, response2, response2_args)]

        super().__init__(prover, verifier, rounds)


def announcement(self, mem, verifier, e_, E_, PI, R_):
    zq = self.group.q
    Rck = zq
    com_ck = self.ck
    Epk = self.elgamal.encrypt

    assert (e_ * Epk[R_](1))[PI] == E_

    mem.a_ = MyArray(j+1 for j in range(m))
    mem.r_a = Rck()
    mem.A = com_ck(mem.a_[PI], mem.r_a)

    self.publish(mem.A, recipient=verifier)


def challenge1(self, mem, prover):
    zq = self.group.q

    mem.A = self.read(sender=prover)

    mem.s = zq()
    mem.t_ = zq[m]()

    self.publish((mem.s, mem.t_), recipient=prover)


def response1(self, mem, verifier, PI):
    zq = self.group.q
    Rck = zq
    com_ck = self.ck

    mem.s, mem.t_ = self.read(sender=verifier)

    mem.b_ = (mem.t_ * mem.s)
    mem.r_b = Rck()
    mem.B = com_ck(mem.b_[PI], mem.r_b)

    self.publish(mem.B, recipient=verifier)


def challenge2(self, mem, prover):
    zq = self.group.q

    mem.B = self.read(sender=prover)

    mem.lamb = zq()

    self.publish(mem.lamb, recipient=prover)


def response2(self, mem, verifier, e_, E_, PI, R_):
    lamb = self.read(sender=verifier)

    c_ = mem.a_ * lamb + mem.b_
    C = mem.A ** lamb * mem.B
    r_c = mem.r_a * lamb + mem.r_b

    cpoke = CPoKE(self, verifier, C, c_, PI, r_c)
    cpoke()

    R = -(R_*mem.b_).sum()
    E = (e_ ** (mem.t_ * mem.s)).product()

    metce = MEtCE(self, verifier, mem.B, E_, E, mem.b_[PI], mem.r_b, R)
    metce()
