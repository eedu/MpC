from .poce import poce as stage02
from .cpoke import cpoke as stage03
from .metce import metce as stage04
from .shuffle import shuffle as stage05


def poce(*args, **kwargs):
    stage02(*args, **kwargs)


def cpoke(*args, **kwargs):
    stage03(*args, **kwargs)


def metce(*args, **kwargs):
    stage04(*args, **kwargs)


def shuffle(*args, **kwargs):
    stage05(*args, **kwargs)
