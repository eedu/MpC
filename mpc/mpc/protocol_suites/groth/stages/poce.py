
from mpc.utils.rng import RNG

from ..protocols.product_of_committed_elements import PoCE
from ..const import m


def poce(self):

    P = self
    q = P.group.q
    group = q
    Rck = RNG(q, group=group)
    zq = RNG(q, group=group)
    com_ck = P.ck

    prover = P[0]
    verifier = P

    a_ = zq[m]()
    r = Rck()

    A = com_ck(a_, r)
    a = a_.product()

    poce = PoCE(prover, verifier, a, A, a_, r)

    poce()
