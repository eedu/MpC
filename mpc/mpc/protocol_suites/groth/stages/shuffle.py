
from mpc.utils.perm import Permutation
from mpc.utils.rng import RNG

from ..protocols.shuffle import Shuffle
from ..const import m


def shuffle(self):
    P = self
    p = RNG(P.group, P.group)
    q = P.group.q
    zp = p
    Gq = zp.order == int(q)
    zq = q
    Rpk = zq
    Epk = self.elgamal.encrypt

    prover = P[0]
    verifier = P

    PI = Permutation(m)

    m_ = Gq[m]()
    e_ = Epk(m_)
    R_ = Rpk[m]()
    E_ = (e_ * Epk[R_](1))[PI]

    shuffle = Shuffle(prover, verifier, e_, E_, PI, R_)
    shuffle()
