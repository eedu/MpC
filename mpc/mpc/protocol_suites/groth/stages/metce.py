
from ..protocols.multi_exponentiation_to_committed_elements import MEtCE
from mpc.utils.rng import RNG
from ..const import m


def metce(self):
    P = self
    p = RNG(P.group, P.group)
    q = P.group.q
    zp = p
    Gq = zp.order == int(q)
    zq = q
    Rck = zq
    Rpk = zq
    Epk = self.elgamal.encrypt
    com_ck = P.ck

    prover = P[0]
    verifier = P

    a_ = zq[m]()
    r = Rck()
    A = com_ck(a_, r)

    m_ = Gq[m]()  # TODO: figure out why
    R_ = Rpk[m]()
    E_ = Epk[R_](m_)

    R = Rpk()
    E = Epk[R](1) * E_ ** a_

    metce = MEtCE(prover, verifier, A, E_, E, a_, r, R)
    metce()
