
from mpc.utils.perm import Permutation
from mpc.utils.rng import RNG

from ..protocols.committed_permutation_of_known_elements import CPoKE
from ..const import m


def cpoke(self):
    P = self
    q = P.group.q
    Rck = RNG(q, group=q)
    zq = RNG(q, group=q)
    com_ck = P.ck

    prover = P[0]
    verifier = P

    a_ = zq[m]()
    PI = Permutation(m)

    r = Rck()
    b_ = a_[PI]
    B = com_ck(b_, r)

    cpoke = CPoKE(prover, verifier, B, a_, PI, r)
    cpoke()
