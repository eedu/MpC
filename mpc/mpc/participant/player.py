from mpc.participant import Participant

import mpc.utils as Utils
from mpc.utils.rng import RNG
from mpc.group import Group, Z, ZGroupMember


class Player(Participant):

    def __init__(self, multiparty, p_id):
        # private storage
        self.b = None
        self._k = None

        super().__init__(multiparty, p_id)

    def k(self, v):
        if self._k is None:
            print("Tried to encrypt without having a key")
            raise RuntimeError

        return v ** self._k

    def k_1(self, v):
        if self._k is None:
            print("Tried to encrypt without having a key")
            raise RuntimeError

        return v ** (self._k_1)

    def privately_store(self, key, value):
        if value is None:
            if key in self._storage:
                del self._storage[key]
        else:
            self._storage[key] = value

    def privately_retrieve(self, key):
        #print(self._storage)
        return self._storage[key]

    def publish(self, content, recipient=all):
        return self.P.message(self, content, recipient)

    def read(self, sender=any):
        return self.inbox.read_last(sender)

    def message(self, sender, content):
        self.inbox.write(sender, content)
