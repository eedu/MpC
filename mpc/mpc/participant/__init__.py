
from mpc.participant.inbox import Inbox
import mpc.utils as Utils

class Participant():

    def __init__(self, multiparty, p_id):
        #private storage
        self._storage = {}
        self.P = multiparty
        self.id = p_id
        self.inbox = Inbox()
        self._repr = None

    def __getattr__(self, attr):
        # Store common values in Participants class, retrieve from there when needed
        if attr in self.P.__dict__:
            return self.P.__dict__[attr]

        print(attr)

        raise RuntimeError

    def __int__(self):
        return self.id

    def __repr__(self):
        if self._repr is None:
            self._repr = "P({})".format(self.id)

        return self._repr

    def k(self, v):
        raise NotImplementedError

    def k_1(self, v):
        raise NotImplementedError

    def compute(self, func, *func_args, **func_kwargs):
        return Utils.run_func_as_method(self, func, *func_args, **func_kwargs)

    def privately_store(self, key, value):
        if value is None:
            if key in self._storage:
                del self._storage[key]
        else:
            self._storage[key] = value

    def privately_retrieve(self, key):
        return self._storage[key]

    def store(self, key, value):
        self.P.store(key, value)

    def retrieve(self, key):
        self.P.retrieve(key)

    def publish(self, content, recipient=all):
        return self.P.message(self, recipient, content)

    def read(self, sender=any):
        return self.inbox.read_last(sender)

    def next(self):
        return self.P[int(self)+1]

    def previous(self):
        return self.P[int(self)-1]
