
class Message:

    def __init__(self, sender, content):
        self.sender = sender
        self.content = content
        self.unread = True

    def read(self):
        if self.unread:
            self.unread = False
        else:
            print("Message already read!")
            raise RuntimeError

class Inbox:

    def __init__(self, child_inboxes=[]):
        self.messages = []
        self.child_inboxes = child_inboxes

    def __repr__(self):
        unread = sum([msg.unread for msg in self.messages])
        return "Inbox ({})".format(unread)

    def write(self, sender, msg):
        for inbox in self.child_inboxes:
            inbox.write(sender, msg)

        self.messages.append(Message(sender, msg))

    def read_last(self, sender=any):
        for msg in reversed(self.messages):
            if sender is any or sender == msg.sender:
                msg.read()
                return msg.content

        return None
            
