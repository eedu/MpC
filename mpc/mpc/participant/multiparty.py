from mpc.group import Group

from mpc.participant.inbox import Inbox
from mpc.group import ZGroupMember


class Multiparty(Group):

    def __init__(self, players, z):
        self._storage = {}
        self.group = z
        self.inbox = Inbox([pi.inbox for pi in players])
        super().__init__(players)

    def compute(self, func, *func_args, **func_kwargs):
        [Pi.compute(func, *func_args, **func_kwargs) for Pi in self]

    def store(self, key, value):
        self._storage[key] = value

    def retrieve(self, key):
        if key not in self._storage:
            return None

        return self._storage[key]

    def message(self, sender, content, recipient=all):
        if recipient is all:
            for player in self:
                player.message(sender, content)
        else:
            recipient.message(sender, content)

    def publish(self, content, recipient=all):
        self.message(self, content, recipient)

    def read(self, sender=any):
        for player in self:
            content = player.inbox.read_last(sender)

        return content

    def rng(self, group):
        P = self

        def int_rng():

            commits = []
            reveals = []

            def int_rng_commit(self):
                ''' here self = Pi'''
                r, r2 = group[2]()
                msg = (r, r2)
                self.privately_store('int_rng_commit', msg)
                commits.append(hash(msg))

            def int_rng_reveal(self):
                ''' here self = Pi'''
                msg = self.privately_retrieve('int_rng_commit')

                reveals.append(msg)

                return commits

            P.compute(int_rng_commit)
            P.compute(int_rng_reveal)
            acc = ZGroupMember(0, group=group)
            for i, msg in enumerate(reveals):
                assert commits[i] == hash(msg)
                acc = acc + msg[0]

            return acc

        return int_rng
