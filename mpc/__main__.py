
protocol = 'groth'


if __name__ == '__main__' and protocol == 'cr':
    print("Setting debug mode")
    import mpc.utils as Utils
    Utils.set_debug_mode()
    print("Importing parameters")
    from mpc.protocol_suites.castella_roca.parameters_helper import p, q, a, s
    from mpc.protocol_suites.castella_roca import CastellaRoca
    n = 10
    cr = CastellaRoca(n, p, q, a, s)

    print("Initiating Protocol Suite")
    cr.initialization()

    print("Shuffling cards")
    cr.card_shuffle()

    print("Drawing first card")
    cr.card_draw(0, 0)

    print("Opening first card")
    cr.card_open(0, 0)

    print("Discarding first card")
    cr.card_discard(0, 0)

if __name__ == '__main__' and protocol == 'groth':
    print("Setting debug mode")
    import mpc.utils as Utils
    Utils.set_debug_mode()
    print("Importing parameters")
    from mpc.protocol_suites.groth.parameters_helper import group
    print("Importing groth")
    from mpc.protocol_suites.groth import Groth
    n = 10
    print("Creating groth")
    ps = Groth(n, group)

    print("Running product of commited elements")
    ps.poce()

    print("Running committed permutation of known elements")
    ps.cpoke()

    print("Multi-exponentiation to Committed Elements")
    ps.metce()

    print("Shuffle")
    ps.shuffle()
